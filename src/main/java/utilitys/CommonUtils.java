package utilitys;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;


/**
 * 工具类，提供公用方法 *
 */
public class CommonUtils {
	/**
	 * 常量：休眠时间，5秒
	 */
    public static final int SLEEP = 5;

    /**
     * 休眠时间 
     * @param time 时间，单位为毫秒
     * @throws Exception 可能抛出异常
     */
    public static void sleep(int time) throws Exception {
    	Thread.sleep(time);
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

}

package page_objects.ios.register;

import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.temporal.ChronoUnit;


public class EnterpriseAccountVerify {

	// 跳過
	@FindBy(name = "略過")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement skipButton;
}



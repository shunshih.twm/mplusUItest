package page_objects.ios.register;

import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.temporal.ChronoUnit;


public class ContactsAlert {

	// 聯絡人權限
	@FindBy(name = "好")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement okButton;
}

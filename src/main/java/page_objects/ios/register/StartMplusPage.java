package page_objects.ios.register;

import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.temporal.ChronoUnit;


public class StartMplusPage {

	// 開始使用 M+
	@FindBy(name = "開始使用 M+")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement startMPlusButton;
}



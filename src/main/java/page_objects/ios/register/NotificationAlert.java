package page_objects.ios.register;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import io.appium.java_client.pagefactory.WithTimeout;

import java.time.temporal.ChronoUnit;


public class NotificationAlert {

	// 傳送通知
	@FindBy(name = "允許")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement okButton;
}

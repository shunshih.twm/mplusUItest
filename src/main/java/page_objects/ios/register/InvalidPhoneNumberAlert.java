package page_objects.ios.register;

import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.temporal.ChronoUnit;


public class InvalidPhoneNumberAlert {

	// 您輸入的電話號碼無效，請再輸入ㄧ次
	@FindBy(name = "您輸入的電話號碼無效")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement InvalidAlert;
}

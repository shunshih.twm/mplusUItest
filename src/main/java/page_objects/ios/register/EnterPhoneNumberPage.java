package page_objects.ios.register;

import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.temporal.ChronoUnit;


public class EnterPhoneNumberPage {

	// 請輸入您的手機號碼
	@FindBy(name = "請輸入您的手機號碼")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement phoneNumberTextField;

	// 同意並進行註冊
	@FindBy(name = "同意並進行註冊")
	@WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
	public WebElement agreeAndRegisterButton;
}



package base;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import Listener.TestFailureListener;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Listeners;
import utilitys.CommonUtils;

import javax.imageio.ImageIO;

/**
 * PO模式iOS Testcase的基类
 *
 */
@Listeners({TestFailureListener.class})
public class TestBase {
	protected static WebDriver driver;

	/**
	 * 获取可用的driver
	 * 
	 * @return WebDriver
	 */
	@SuppressWarnings("rawtypes")
	public static WebDriver getDriver() {
		if (driver == null) {
			// set up appium
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("xcodeOrgId", "K3DPR3N279");
			capabilities.setCapability("xcodeSigningId", "iPhone Developer");
			capabilities.setCapability("platformName", "iOS");
			//capabilities.setCapability("platformVersion", "11.4.1");
			//capabilities.setCapability("deviceName", "Pt-iphone8");
			//capabilities.setCapability("udid", "92996cb1cf19ab017174f1e8555c94dade94c2ec");

			capabilities.setCapability("platformVersion", "12.0.1");
			capabilities.setCapability("deviceName", "TWM-PT_iPhone");
			capabilities.setCapability("udid", "e17873944bd2d4d099711920cdb7856c6377bcde");

			//capabilities.setCapability("app", "/Users/Shun/Desktop/MP_E_Staging.app");
			capabilities.setCapability("app", "/Users/Shun/Desktop/20181126_M+_E_Staging_Log_99.5.6.0.01.ipa");
			capabilities.setCapability("updatedWDABundleId", "com.mplusapp.mplus.ShareExtension");

			capabilities.setCapability("bundleId", "com.terntek.mplus");
			capabilities.setCapability("automationName", "XCUITest");

			capabilities.setCapability("showXcodeLog", true);
			//capabilities.setCapability("takesScreenshot", true);
			capabilities.setCapability("fullReset", true);
			capabilities.setCapability("useNewWDA", true);

			try {
				driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Exception: "+ e.getMessage());
				e.printStackTrace();

			}
		}
		return driver;
	}

	/**
	 * 每个测试方法开始执行前执行该方法：获取driver并利用其打开首页
	 * 
	 * @throws Exception
	 *             可能抛出异常
	 */
	@BeforeClass
	public void setUp() throws Exception {
		driver = getDriver();
		driver.manage().timeouts().implicitlyWait(CommonUtils.SLEEP, TimeUnit.SECONDS);
	}

	/**
	 * 每个测试方法结束执行后执行该方法：退出driver并置空
	 * 
	 * @throws Exception
	 *             可能抛出异常
	 */
	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		driver = null;
	}



	public String screenshot(String filePath,String fileName) throws IOException {

//		File srcFile=driver.getScreenshotAs(OutputType.FILE);
//		String filename=UUID.randomUUID().toString();
//		String timeStr = CommonUtils.getCurrentTime();
//		File targetFile=new File(filePath + filename +".jpg");
//		FileUtils.copyFile(srcFile,targetFile);

		try {
			File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			BufferedImage screenshotBase64=ImageIO.read(screenshot);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(screenshotBase64, "jpg", Base64.getEncoder().wrap(os));
			return os.toString(StandardCharsets.ISO_8859_1.name());

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}

package Listener;

import base.TestBase;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import utilitys.CommonTestUtils;

import java.io.IOException;

public class TestFailureListener extends TestListenerAdapter {
    @Override
    public void onTestFailure(ITestResult tr) {
        super.onTestFailure(tr);

        String testClassName = tr.getTestClass().getName();
        String throwableLocalizedMessage = tr.getThrowable().getLocalizedMessage();

        //Reporter.log("<a href='"+ destFile.getAbsolutePath() + "'> <img src='"+ destFile.getAbsolutePath() + "' height='100' width='100'/> </a>");
        try {
            String filePath = CommonTestUtils.screenshot();

            System.setProperty("org.uncommons.reportng.escape-output", "false");

            Reporter.log("<h2>"+ testClassName +"</h2>");
            Reporter.log("<h3>"+ throwableLocalizedMessage +"</h3>");
            Reporter.log("<h3>"+ filePath +"</h3>");


            String imageURL = "http://localhost:8080/job/m_plus/ws/" + filePath;
            Reporter.log(
                    "<a title=\"title\" href=\"" + imageURL + "\">" + "<img style=\"height: auto; width: 50vw;\" alt=\"alternativeName\" title=\"title\" src=\"" + imageURL + "\"> </a>");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

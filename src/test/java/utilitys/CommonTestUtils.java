package utilitys;

import base.TestBase;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;


/**
 * 工具类，提供公用方法 *
 */
public class CommonTestUtils {

    public static String screenshot() throws IOException {



        try {
//            File screenshot = ((TakesScreenshot)TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
//            BufferedImage screenshotBase64=ImageIO.read(screenshot);
//            ByteArrayOutputStream os = new ByteArrayOutputStream();
//            ImageIO.write(screenshotBase64, "png", Base64.getEncoder().wrap(os));
// return os.toString(StandardCharsets.ISO_8859_1.name());

            File srcFile=((TakesScreenshot)TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
            String filename=UUID.randomUUID().toString();
            String timeStr = CommonUtils.getCurrentTime();

            String basePath = System.getProperty("user.dir") ;
            String imageFolderPath = "/screenshots/" + timeStr + "/";
            CommonTestUtils.createFolder(basePath);
            String filePath = imageFolderPath + filename + ".jpg";

            File targetFile=new File(basePath + filePath);
            FileUtils.copyFile(srcFile,targetFile);

            return filePath;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void createFolder(String path)  {

        File theDir = new File(path);

// if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
    }
}

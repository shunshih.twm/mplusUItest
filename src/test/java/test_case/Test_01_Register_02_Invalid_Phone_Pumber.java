package test_case;

import base.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page_objects.ios.register.*;
import utilitys.CommonUtils;


public class Test_01_Register_02_Invalid_Phone_Pumber extends TestBase {
	@Test
	public void test_01_Register() throws Exception {

		// 傳送通知
		NotificationAlert notificationAlert = PageFactory.initElements(driver, NotificationAlert.class);
		notificationAlert.okButton.click();

		// 電話簿權限
		PhoneBookAlert phoneBookAlert = PageFactory.initElements(driver, PhoneBookAlert.class);
		phoneBookAlert.okButton.click();

		// 聯絡人權限
		ContactsAlert contactsAlert = PageFactory.initElements(driver, ContactsAlert.class);
		contactsAlert.okButton.click();

		// 請輸入您的手機號碼
		EnterPhoneNumberPage enterPhoneNumberPage = PageFactory.initElements(driver, EnterPhoneNumberPage.class);
		enterPhoneNumberPage.phoneNumberTextField.sendKeys("12345678");
		enterPhoneNumberPage.agreeAndRegisterButton.click();

		// 您輸入的電話號碼無效，請再輸入ㄧ次
		InvalidPhoneNumberAlert invalidPhoneNumberAlert = PageFactory.initElements(driver, InvalidPhoneNumberAlert.class);
		boolean isExist = invalidPhoneNumberAlert.InvalidAlert != null;

		Assert.assertEquals(isExist ,true,"找不到該元件(您輸入的電話號碼無效，請再輸入ㄧ次)");

		CommonUtils.sleep(5000);
	}



}

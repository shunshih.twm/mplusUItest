package test_case;

import base.TestBase;
import org.testng.annotations.Test;
import org.openqa.selenium.support.PageFactory;

import page_objects.ios.register.*;
import utilitys.CommonUtils;


public class Test_01_Register_01_Success extends TestBase {
	@Test
	public void test_01_Register() throws Exception {

		System.out.print("test_01_Register");
		// 傳送通知
		NotificationAlert notificationAlert = PageFactory.initElements(driver, NotificationAlert.class);
		notificationAlert.okButton.click();

		// 電話簿權限
		PhoneBookAlert phoneBookAlert = PageFactory.initElements(driver, PhoneBookAlert.class);
		phoneBookAlert.okButton.click();

		// 聯絡人權限
		ContactsAlert contactsAlert = PageFactory.initElements(driver, ContactsAlert.class);
		contactsAlert.okButton.click();

		// 請輸入您的手機號碼
		EnterPhoneNumberPage enterPhoneNumberPage = PageFactory.initElements(driver, EnterPhoneNumberPage.class);
		enterPhoneNumberPage.phoneNumberTextField.sendKeys("0944444443");
		enterPhoneNumberPage.agreeAndRegisterButton.click();

		// 簡訊認證碼
		GetVerifyCodeAlert getVerifyCodeAlert = PageFactory.initElements(driver, GetVerifyCodeAlert.class);
		getVerifyCodeAlert.okButton.click();

		// 請輸入認證碼
		EnterVerifyCodePage enterVerifyCodePage = PageFactory.initElements(driver, EnterVerifyCodePage.class);
		enterVerifyCodePage.verifyCodeTextField.sendKeys("6910");
		enterVerifyCodePage.okButton.click();

		//開始使用 M+
		StartMplusPage startMplusPage = PageFactory.initElements(driver, StartMplusPage.class);
		startMplusPage.startMPlusButton.click();

		// 輸入暱稱
		EnterNamePage enterNamePage = PageFactory.initElements(driver, EnterNamePage.class);
		enterNamePage.nameField.sendKeys("測試暱稱Test");
		enterNamePage.okButton.click();

		// 跳過 tutorial
		TutorialPage tutorialPage = PageFactory.initElements(driver, TutorialPage.class);
		tutorialPage.startMPlusButton.click();


		CommonUtils.sleep(5000);
	}

}
